
import UIKit

class SignUpViewController: UIViewController, SignUpView {

    @IBOutlet weak var txtmail: UITextField!
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    @IBOutlet weak var txtphoneNumber: UITextField!
    
    var singUpPresenter:SignUpPresenter = SignUpPresenter()!
    
    func SignUpSuccess() {
        print("thanh cong")
    }
    
    func SignUpAcountExists() {
        print("tai khoan da ton tai")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.singUpPresenter.attachView(self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func dangKy(_ sender: Any) {
        let dangky = Register(email: txtmail.text! ,name: txtname.text! ,password: txtpassword.text! , phoneNumber: txtphoneNumber.text!)
        print("dangKy \(dangky)")
        singUpPresenter.handelSignUp(register: dangky)
        
    }
    
}

