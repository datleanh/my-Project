//
//  SignUpPresenter.swift
//  eventomorrow-ios
//
//  Created by Nguyen Ngoc Son on 1/17/18.
//  Copyright © 2018 Nguyen Ngoc Son. All rights reserved.
//

import Foundation

protocol SignUpPresenterImp {
    func handelSignUp(register:Register)
}

protocol SignUpView {
    func SignUpSuccess()
    func SignUpAcountExists()
}

class SignUpPresenter: SignUpPresenterImp {
    var signUpView:SignUpView?
    
    init?() {
    }
    
    func attachView(_ view: SignUpView) {
        self.signUpView=view
    }
    
    let signUpService = SignUpService()
    func handelSignUp(register: Register) {
        signUpService.getDataSignUp(register: register) { result in
            switch result{
            case "ACCOUNT_CREATED":
                self.signUpView?.SignUpSuccess()
            case "ACCOUNT_EXISTS":
                self.signUpView?.SignUpAcountExists()
            default:
                print("")
            }
        }
    }
}
