//
//  ViewController.swift
//  Eventomorrow
//
//  Created by Nguyen Ngoc Son on 1/15/18.
//  Copyright © 2018 Nguyen Ngoc Son. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,LoginNormalView, LoginFacebookView,UITextFieldDelegate{
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
   
    @IBOutlet weak var label_ValidationMessage: UILabel!
   
    @IBOutlet weak var scrollView: UIScrollView!
    
    var loginFacebookPresenter = LoginFacebookPresenter()
    var loginNormalPresenter: LoginNomalPresenter = LoginNomalPresenter()!
    
    
    func messageLoginFacebookSuccess(){
        let alertController = UIAlertController(title: "", message: "Login Facebook Successful", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default){ action in
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as UIViewController, animated: true, completion: nil)
            })
        present(alertController, animated: true, completion: nil)
    }
        
    func messageUnauthorizedFacebook(){
        let alertController = UIAlertController(title: "", message: "Login Facebook Failed! Unauthorized Error", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ action in
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as UIViewController, animated: true, completion: nil)
        })
        present(alertController, animated: true, completion: nil)
    }
    func messageFobiddenFacebook(){
        let alertController = UIAlertController(title: "", message: "Login Facebook Failed! Fobidden Error", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ action in
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as UIViewController, animated: true, completion: nil)
        })
        present(alertController, animated: true, completion: nil)
    }
    func messageServerErrorLogInFacebook(){
        let alertController = UIAlertController(title: "", message: "Login Facebook Failed! Internal error on Server", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ action in
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as UIViewController, animated: true, completion: nil)
        })
        present(alertController, animated: true, completion: nil)
    }
    func messageCanNotGetFaceId(){
        let alertController = UIAlertController(title: "", message: "Login Facebook Failed! Can not get FacebookID", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){ action in
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as UIViewController, animated: true, completion: nil)
        })
        present(alertController, animated: true, completion: nil)
    }
    
    func messageLoginSuccess() {
        let alertController = UIAlertController(title: "", message: "Login Facebook Successful", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default){ action in
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as UIViewController, animated: true, completion: nil)
        })
        present(alertController, animated: true, completion: nil)
//        print("Login Success")
    }
    
    func messageUnauthorizedAccount() {
//        print("Unauthorized")
        let alert = UIAlertController(title: "Login Notification", message: NSLocalizedString("logiUnauthorizedNotification", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert,animated: true,completion: nil)
    }
    
    func messageFobiddenAccount() {
//        print("FobbidenAcount")
        let alert = UIAlertController(title: "Login Notification", message: NSLocalizedString("loginBannedAccountNotification", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert,animated: true,completion: nil)
    }
    
    func messageServerError() {
//        print("serverError")
        let alert = UIAlertController(title: "Login Notification", message: NSLocalizedString("loginBannedAccountNotification", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert,animated: true,completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginNormalPresenter.actachView(self)
        self.loginFacebookPresenter.attachView(self)
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        addKeyboardObserver()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // Scroll when show hide Keyboard
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    @IBAction func actionValidateEmail(_ sender: Any) {
        let emailValidate = txtEmail.text!
                if self.loginNormalPresenter.checkEmailValidation(email: emailValidate)
                {
                    self.label_ValidationMessage.text = ""
                }else {
                    self.label_ValidationMessage.text = "Email is wrong!"
                }
       
    }
    
    @IBAction func btn_LoginbyAccount(_ sender: Any) {
        let email = txtEmail.text!
                let password = txtPassword.text!
                self.loginNormalPresenter.handelLoginNormal(email: email, password: password)
    }
    @IBAction func hideKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
//    @IBAction func btn_LoginbyAccount(_ sender: Any) {
//        let email = txtEmail.text!
//        let password = txtPassword.text!
//        self.loginNormalPresenter.handelLoginNormal(email: email, password: password)
//    }
    
    
    @IBAction func btnLoginFacebook_Clicked(_ sender: UIButton) {
        self.loginFacebookPresenter.handleLoginFacebook(loginViewController: self)
        LoadingOverlay.shared.show(view: self.view)
    }
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//
//    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
