//
//  LoginNormalModel.swift
//  DemoAlamofire
//
//  Created by Le Anh Dat on 1/16/18.
//  Copyright © 2018 Le Anh Dat. All rights reserved.
//

import Foundation
import Alamofire

class LoginNormalService {
    let url = "http://192.168.81.174:8080/auth/login"
    func checkLogin( email:String, password:String, completion: @escaping (Int?,String?) -> Void) {
        let parameters: [String : String] = ["email": email,"password":password]
        Alamofire.request(url, method: .post,parameters: parameters,encoding: JSONEncoding.default).responseJSON
            { response in
           
                let statusCode = response.response?.statusCode
            

                switch response.result {
                case .failure(let error) : print(error)
                completion(statusCode, "")
                    
                case .success(_):
                    
                    if let json = response.result.value as? [String : Any]{
                      
                        if statusCode == 200 , let token = json["token"] as? String
                        {
                            completion(statusCode, token)
                        }
                        else {
                            completion(statusCode, nil)
                        }
                    }
             
                }
                
                
        }
    }
    
    // Code from now
    
//    public func parseJsonProfile(data:String) -> Profile{
//        var profile = Profile()
//        return profile
//    }
}



