//
//  LoginNomalPresenter.swift
//  DemoAlamofire
//
//  Created by Le Anh Dat on 1/16/18.
//  Copyright © 2018 Le Anh Dat. All rights reserved.
//

import Foundation

protocol LoginNormalView {
    func messageLoginSuccess()
    func messageUnauthorizedAccount()
    func messageFobiddenAccount()
    func messageServerError()
}

protocol LoginNormalPresenterImp {
     func handelLoginNormal(email:String, password:String)
}


class LoginNomalPresenter: LoginNormalPresenterImp{
    var loginNormalView: LoginNormalView?
    
    init?() {
    }
    
    func actachView(_ view: LoginNormalView) {
        self.loginNormalView = view
    }
    func checkEmailValidation(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    func handelLoginNormal(email: String, password: String ) {
        let loginNormalService = LoginNormalService()
        loginNormalService.checkLogin(email: email, password: password) { statusCode,token  in
            guard let code = statusCode else { return }
                switch code {
            case 401:
                self.loginNormalView?.messageUnauthorizedAccount()
            case 403:
                self.loginNormalView?.messageFobiddenAccount()
            case 500:
                self.loginNormalView?.messageServerError()
            case 200:
                self.loginNormalView?.messageLoginSuccess()
                guard let tokenLogin = token else {return}
                self.saveTokentoDevice(token: tokenLogin)
            default:
                print("nil")
            }
        }
        

    }
    func saveTokentoDevice(token:String)-> Void{
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "loginToken")
    }
    func getToken()-> String?{
        let defaults = UserDefaults.standard
        if let token = defaults.string(forKey: "loginToken")
        {
            return token
        } else {return nil}
    }
    
}

