//
//  LoginPresenter.swift
//  eventomorrow-ios
//
//  Created by Vo The Dong An on 1/16/18.
//  Copyright © 2018 Nguyen Ngoc Son. All rights reserved.
//

import Foundation
protocol LoginFacebookView{
    func messageLoginFacebookSuccess()
    func messageUnauthorizedFacebook()
    func messageFobiddenFacebook()
    func messageServerErrorLogInFacebook()
    func messageCanNotGetFaceId()
}


protocol LoginPresenterFacebookProtocol{
    func handleLoginFacebook(loginViewController: LoginViewController)
}


class LoginFacebookPresenter: LoginPresenterFacebookProtocol {
    
    fileprivate var loginView:LoginFacebookView?
    
    func handleLoginFacebook(loginViewController: LoginViewController) {
        LoginFacebookService.shared.loginByFacebook(loginViewController: loginViewController){ result in
            guard let status = result.0 else {
                return
            }
            switch status {
            case 200:
                LoadingOverlay.shared.hide()
                self.loginView?.messageLoginFacebookSuccess()
            case 401:
                LoadingOverlay.shared.hide()
                self.loginView?.messageUnauthorizedFacebook()
            case 403:
                LoadingOverlay.shared.hide()
                self.loginView?.messageFobiddenFacebook()
            case 404:
                LoadingOverlay.shared.hide()
                self.loginView?.messageCanNotGetFaceId()
            case 500:
                LoadingOverlay.shared.hide()
                self.loginView?.messageServerErrorLogInFacebook()
            default:
                LoadingOverlay.shared.hide()
                self.loginView?.messageServerErrorLogInFacebook()
            }
        }
        guard let token = LoginFacebookService.shared.user?.token else {
            return
        }
    }
    
    func attachView(_ view: LoginFacebookView){
        loginView = view
    }
}
